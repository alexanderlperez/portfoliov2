module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    'ftp-deploy': {
      build: {
        auth: {
          host: '192.185.4.119',
          port: 21,
          authKey: 'key1'
        },
        src: 'deploy/',
        dest: '/www',
        exclusions: ['sass/', 'includes/', '*.jade', '.*', '*.rb', '*.vim', 'Gruntfile*']
      }
    }
  });

  grunt.loadNpmTasks('grunt-ftp-deploy');

  grunt.registerTask('default', 'ftp-deploy');

};
