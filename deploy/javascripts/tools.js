
function generateNewField(array) {
  array.forEach(function (v, x) {
    array[x].forEach(function (v, y) {
      array[x][y] = Math.round(Math.random());
    });
  });
}


function drawCells(field, cellSize) {
  //draw the cells
  ctx.fillStyle = "lightgray";
  field.forEach(function (v, x, a) {
    field[x].forEach(function (v, y, a) {
      if (field[x][y] == 1) {
        ctx.rect(x*cellSize, y*cellSize, cellSize, cellSize);
      }
    });
  });
  ctx.fill();
}

//assumes the image will be pixelated according to the cellSize
function createFieldMask(width, height, cellSize) {
  var mask = createNew2dArray(width, height, 0);
  var pixelData;

  mask.forEach(function (v, x, a) {
    mask[x].forEach(function (v, y, a) {
      pixelData = ctx.getImageData(x*cellSize, y*cellSize, 1, 1).data;
      if (pixelData[3] > 0) {
        mask[x][y] = 1;
      }
    });
  });

  return mask;
}

function createNew2dArray(w, h, initTo) {
  var arr = [];

  //initialize the cellField
  for (var y = 0; y < h; y++) {
    arr[y] = [];
    for (var x = 0; x < w; x++) {
      arr[y][x] = 0;
    }
  }

  return arr;
}
