var ctx,
    canvas,
    refreshRate = 15,
    offsetX = -6,
    offsetY = 0,
    width = 400,
    height = 400;

var cellSize = 5,
    addedCells = 35;

var fieldWidth =  width / cellSize,
    fieldHeight = height / cellSize,
    cellField = [];

var image = new Image(),
    imgsrc = 'imgs/face.png';
var fieldMask;
var cellCount = 0;
var gAlpha = .2;


function init () {

  //initialize the cellField
  cellField = createNew2dArray(fieldWidth, fieldHeight, 0);

    //ctx = canvas.getContext('2d');
    if(document.getCSSCanvasContext) {
      ctx = document.getCSSCanvasContext('2d', 'bg', 400, 400);
    } else {
      canvas = document.createElement('canvas');
      canvas.id = 'face';
      canvas.width = '400'
      canvas.height = '400';
      ctx = canvas.getContext('2d');
      document.getElementsByClassName('contact')[0].appendChild(canvas);
    }

    image.onload = function () {
      ctx.drawImage(image, 0, 0, 400, 400);
      //fieldMask = createFieldMask(fieldWidth, fieldHeight, cellSize);
      generateNewField(cellField);
      setInterval(function () { 
        if(window.requestAnimationFrame){
          requestAnimationFrame(refresh);
        } else {
          refresh();
        }
        }, 1000/refreshRate);            
    };
    //load the bg image and have the onload fire
    image.src = imgsrc;

    //interaction, is looking for container .face
    var container = document.getElementById('face');
    container.addEventListener('click', function () { addCells(cellField, 25); });
    container.addEventListener('mouseover', function () { gAlpha = .6; });
    container.addEventListener('mouseout', function () {gAlpha = .2; });
}

//the core animation loop functions
function refresh() {
  clear();
  update();
}

function update() {
  addCells(cellField, addedCells);
  cellField = updateCells(cellField);
  draw();
}

//erase the canvas
function clear() {
    ctx.clearRect(0,0,width,height);
}

//draw each cell    
function draw() {
  ctx.save();
  ctx.beginPath();
  //draw the new cells
  drawCells(cellField, cellSize);
  //clip the image to be revealed
  ctx.clip();
  ctx.drawImage(image, 0, 0, 400, 400);
  ctx.restore();
  ctx.save();
  ctx.globalAlpha = gAlpha;
  ctx.drawImage(image, 0, 0, 400, 400);
  ctx.restore();
}

window.onload = init();
