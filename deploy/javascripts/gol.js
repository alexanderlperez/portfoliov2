
function addCells(field, num) {
  var x, y;
  for (var i = 0; i < num; i++) {
    x = Math.abs(Math.round(Math.random()*fieldWidth - 1));
    y = Math.abs(Math.round(Math.random()*fieldHeight - 1));
    try {
    field[x][y] = 1;
    } catch (err) {
      console.log(err.message, "|", x, y);
    }
  }
}


function _countNeighbours(x, y) {
    var amount = 0;
    
    function _isFilled(x, y) {
        return cellField[x] && cellField[x][y];
    }
    
    if (_isFilled(x-1, y-1)) amount++;
    if (_isFilled(x,   y-1)) amount++;
    if (_isFilled(x+1, y-1)) amount++;
    if (_isFilled(x-1, y  )) amount++;
    if (_isFilled(x+1, y  )) amount++;
    if (_isFilled(x-1, y+1)) amount++;
    if (_isFilled(x,   y+1)) amount++;
    if (_isFilled(x+1, y+1)) amount++;
    
    return amount;
}

function updateCells (field) {
  //create a new array
  //run the previous cellField through the countNeighbors algo
  //save the results in the new array
  //overwrite the old with the new array
  var resultsArr = [];

  field.forEach(function(row, x) {
      resultsArr[x] = [];
      row.forEach(function(cell, y) {
          var alive = 0,
              count = _countNeighbours(x, y);
          
          if (cell > 0) {
              alive = count === 2 || count === 3 ? 1 : 0;
          } else {
              alive = count === 3 ? 1 : 0;
          }
          
          resultsArr[x][y] = alive;
      });
  });
  
  return resultsArr;

}
