<?php
  #
  $email = $_POST['email'];
  $subj = $_POST['subject'];
  $message = $_POST['msg-txt'];

  // Enter here your email, where you want to receive all the messages from 'Send us message' form 
  $to = 'alexanderlperez@gmail.com';

  // Enter here subject that is relevant to you page (e.g. Message from coming soon page...)
  $subject = 'Message from alexanderlperez.com';
  $message = "RE: ".$subj."\nEmail: ".$email."\nMessage: ".$message;

  // input here some email address that will be relevant to this webpage 
  $headers = "From: contact@alexanderlperez.com" . "\r\n";
 
  if (filter_var($email, FILTER_VALIDATE_EMAIL))
  { 
    mail($to, $subject, $message, $headers); 
    echo "1"; // success return value
  }
  else
  {
    echo "0"; // error return value
  }
?>
